##########################################################################
#' Fonction de calcul de la feuille définitive
#' @description  régle Fobs=>Fdev par Arvalis
#' travaux SynOEM - Jacques Veslot (Acta,2015-2016)
#' @param df : table extrait de vigiculture
calc_fdef <- function(df)
{
  df$diff <- NA
  df$diff[df$stade_num >= 39] <- 0
  df$diff[df$stade_num == 37] <- 1
  df$diff[df$stade_num == 32] <- 2
  df$diff[df$stade_num == 31] <- 3
  
  factor(
    paste("F", as.numeric(substring(df$fobs,2,2)) + df$diff, sep=""),
    levels=paste("F", 1:4, sep=""))
}
##########################################################################
# exemple pour utilisation stage INRIA
# récupération données parcelles observations-vigicultures-v2
apiagro.key = "46e8c524a9a18ac7aa720b546cdfc1d67073a855c77ad2882226cbe6"
#list_db = c("bsv_vigne_maladies_epicure", "bsv_vigne_ravageurs_epicure","bsv_vigne_pieges_epicure")
db = "observations-vigicultures-v2"
url1= "https://plateforme.api-agro.fr/explore/dataset/"
url1= paste(url1,db ,sep="")
url1= paste(url1,"/download/?format=csv&timezone=Europe/Berlin&use_labels_for_header=true" ,sep="")
url1= paste(url1, "&apikey=", apiagro.key,sep="")

# test en dwnl uniquement les variablle rouille Jaune
url1= paste(url1, "&q=id_variable=2875 OR id_variable=2876 OR id_variable=2877 ",sep="")
system.time(TAB <- read.csv(url(url1), sep = ";", header = T, encoding ="UTF-8",stringsAsFactors=FALSE))

unique(TAB$libelle_variable)

# stade en numerique
TAB$stade_num = as.numeric(substring((TAB$libelle_stade),2,3))

# Feuille observee :
TAB$fobs = factor(NA, levels=paste("F", 1:3, sep=""))
# pour la Rouille Jaune
select = TAB$id_variable==2875 | TAB$id_variable==2876 | TAB$id_variable==2877
TAB[select,"fobs"] <-  factor(gsub("Rouille Jaune ","",TAB[select,"libelle_variable"]),
                     levels=paste("F", 1:3, sep=""))
# Feuille définitive :
TAB$fdef <- calc_fdef(TAB)

head(TAB)
write.csv(TAB, file="observations-vigicultures-v2+fd.csv")
