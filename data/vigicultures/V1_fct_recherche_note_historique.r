## fonction pour récupérer une note de sensibilité variétale de l'année de récolte 
## créateur Fabrice Moreau
## modification : Emmanuelle Gourdain, pour la rendre plus générique

## BDD avec une colonne avec le nom de la variété et l'année 
## varcom : export historique de varcom

## attention à la casse des noms de variétés qui doivent être homogènes entre varcom et bdd
## la fonction gère les intitulés de colonnes 


TrouveNoteSensibilite <- function(bdd, varcom, varcom_annee, varcom_variete, varcom_maladie, varcom_note, bdd_variete, bdd_annee, nom_maladie, note_maladie){

	
  #on corrige les noms de colonnes de la base varcom 
  colnames(varcom)[which(colnames(varcom) == varcom_annee)] <- "Annee"
  colnames(varcom)[which(colnames(varcom) == varcom_variete)] <- "Variete"
  #on récupère la maladie ciblée
  variete_sensibilites <- varcom[varcom[ , varcom_maladie] == nom_maladie, ]
  
  bdd$variete <- as.character(bdd[, bdd_variete])
  bdd$anneerecolte <- as.numeric(as.vector(bdd[, bdd_annee]))
  bdd[,paste0(note_maladie, "_hist")] <- NA
  
  for(i in 1:nrow(bdd)) 
  {
    lignes <- variete_sensibilites[variete_sensibilites$Variete == bdd$variete[i], ]
    if (dim(lignes)[1] > 0) 
	{
      lignes <- lignes[order(lignes$Annee, decreasing = TRUE),]
      ok <- lignes[lignes$Annee <= bdd$anneerecolte[i],] 
      if (dim(ok)[1] > 0)
        bdd[,paste0(note_maladie, "_hist")][i] <- abs(as.numeric(as.character(ok[1, varcom_note])))    
      else
        bdd[,paste0(note_maladie, "_hist")][i] <- NA
    }
    else
      bdd[,paste0(note_maladie, "_hist")][i] <- NA 
  }
  return(bdd)
}