"""

  Set covering in Google CP Solver.

  Problem from
  Katta G. Murty: 'Optimization Models for Decision Making', page 302f
  http://ioe.engin.umich.edu/people/fac/books/murty/opti_model/junior-7.pdf

  6 rules make different prediction errors w.r.t. 10 data points.

  The objective is to select a set of rules covering as many data points
  as possible with minimal error.

"""
from __future__ import print_function
from ortools.constraint_solver import pywrapcp
import numpy as np
import math


def main(unused_argv):

  # Create the solver.
  solver = pywrapcp.Solver("Set covering")

  #
  # data
  #
  num_rules = 6
  num_data_points = 10
  k = 3 # Number of rules we want in the solution

  
  # which rule does a data point belong to?
  belongs = [
      [1, 1, 1, 1, 1, 0, 0, 0, 0, 0],   # rule 1 
      [0, 0, 0, 0, 0, 1, 1, 1, 1, 1],   # rule 2 
      [0, 1, 1, 0, 0, 0, 0, 1, 1, 1],   # rule 3 
      [1, 0, 0, 0, 1, 1, 1, 0, 0, 0],   # rule 4 
      [0, 0, 1, 1, 1, 1, 1, 0, 1, 0],   # rule 5 
      [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]    # rule 6, baseline rule
  ]

  # how much error a rule makes for a data point
  # -1 means the rule does not make predictions about the data point
  error = [
      np.array([0.002, 0.001, 0.003, 0.2, 0.1, -1, -1, -1, -1, -1]),   # rule 1 
      np.array([-1, -1, -1, -1, -1, 0.3, 0.22, 0.3, 0.0002, 0.1]),     # rule 2 
      np.array([-1, 0.5, 0.001, -1, -1, -1, -1, 0.023, 0.78, 0.0004]), # rule 3 
      np.array([0.4, -1, -1, -1, 0.2, 0.003, 0.000087, -1, -1, -1]),    # rule 4 
      np.array([-1, -1, 0.91, 0.0001, 0.00032, 0.00028, 0.0000127, -1, 1.2, -1]),    # rule 5 
      np.array([0.1, 0.89, 0.56, 0.34, 0.23, 0.11, 0.009, 2.6, 0.003, 0.002])    # rule 6, baseline rule
  ]

  # calculate the weights for the rules
  average_error_per_rule = np.array([0.0 for i in range(num_rules)])
  
  # problem here: the objective function accepts only integer weights, so 
  # we have to convert the errors to integers  
  for i in range(num_rules) :
    average_error_per_rule[i] = np.mean(error[i][np.sign(error[i]) >= 0])

  min_error = np.min(average_error_per_rule)
  # note: I assuming a non-zero average error: just for the sake of this
  # example
  scale_factor = 1.0
  if min_error < 1.0 :
    scale_factor = math.floor(math.log10(min_error))

  scale_factor = abs(math.floor(math.log(min_error))) # math.ceil would do the trick with less precision
  weights = np.rint(average_error_per_rule * pow(10, scale_factor)) 
  weights = weights.astype(int)

  #
  # declare variables
  #
  x = [solver.IntVar(0, 1, "x[%i]" % i) for i in range(num_rules)]

  #
  # constraints
  #
  
  # objective function: dot product between the vector of 
  # variables and the vector of weights
  z = solver.ScalProd(x, weights.tolist())

  # ensure that we take at most k rules
  solver.Add(
        solver.SumLessOrEqual([x[i] for i in range(num_rules)], k))
  
  # ensure that each data point is covered by at least
  # one rule
  for j in range(num_data_points):
        solver.Add(
            solver.SumGreaterOrEqual([x[i] * belongs[i][j]
                                      for i in range(num_rules)],
                                     1))

  objective = solver.Minimize(z, 1)

  #
  # solution and search
  #
  solution = solver.Assignment()
  solution.Add(x)
  solution.AddObjective(z)

  collector = solver.LastSolutionCollector(solution)
  solver.Solve(solver.Phase(x,
                            solver.INT_VAR_DEFAULT,
                            solver.INT_VALUE_DEFAULT),
               [collector, objective])

  print("z (objective function):", collector.ObjectiveValue(0))
  print("x:", [collector.Value(0, x[i]) for i in range(num_rules)])
  for i in range(num_rules):
    if collector.Value(0, x[i]) == 1:
      print('We have taken rule #', i, ':' , belongs[i])

  print()


if __name__ == "__main__":
  main("cp sample")
