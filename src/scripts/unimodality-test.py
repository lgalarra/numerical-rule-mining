#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
from pyfolding import batch_folding_test
from pyfolding import batch_folding_test_cpp

import sys


if sys.argv[1].startswith('-i') : 	
	cpxrPath = sys.argv[1][2:]
	startIdx = 2
else :
	cpxrPath = '/home/luis/Documents/cpxr'
	startIdx = 1

print('Using CPXR library from', cpxrPath)	
sys.path.insert(0, cpxrPath)

from cpxr import loadCSVData

for path in sys.argv[startIdx:] :
	data = loadCSVData(path)
	numericalCols = data.select_dtypes(include=[np.number]).columns.tolist()
	X = data[numericalCols].dropna().values
	#print(X)
	print('Dataset:', path)
	print('C++ test:', batch_folding_test_cpp(X))
	print('Python test:', batch_folding_test(X))
