#!/usr/bin/python3

## This script and module converts the original JSON files provided by 
## IFV and turns them into arff files that can be processed by the data 
## mining algorithms

import os
import sys
import json
import codecs
import importlib
import time
import datetime
import csv
import numpy as np
import math

def slopes(values) :
    slopes = []
    for i in range(1, len(values)) :
        slopes.append((values[i][1] - values[i - 1][1]) / (values[i][0] - values[i - 1][0]))
        
    return slopes

def meanSlope(values) :
    return np.mean(slopes(values))

def maxSlope(values):
    return np.max(slopes(values))

def calculateHistorical(attribute, value, index, startDate, parcelle, ndays, aggFunction):
    timeStamp = time.mktime(datetime.datetime.strptime(startDate, "%Y-%m-%d").timetuple())
    points = [float(value)]
    for i in range(0, ndays) :
        timeStamp = timeStamp - 24 * 3600
        newDate = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d')
        if newDate in index :
            if parcelle in index[newDate] :
                if attribute in index[newDate][parcelle] :
                    points.append(float(index[newDate][parcelle][attribute])) 
        
    if len(points) > 0 :
        return aggFunction(points)
    else:
        return float('NaN')

def calculateHistoricalSlope(attribute, value, index, startDate, parcelle, ndays, aggFunction):
    timeStamp = time.mktime(datetime.datetime.strptime(startDate, "%Y-%m-%d").timetuple())
    
    points = [(timeStamp, float(value))]
    
    for i in range(1, ndays) :
        timeStamp = timeStamp - 24 * 3600
        newDate = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d')
        if newDate in index :
            if parcelle in index[newDate] :
                if attribute in index[newDate][parcelle] :
                    points.append((timeStamp / (24 * 3600), float(index[newDate][parcelle][attribute])))
        
    if len(points) > 1 :
        return aggFunction(points)
    else:
        return float('NaN')


def getReferenceDate(currentDate):
    dateParts = currentDate.split('-')
    year = int(dateParts[0]) - 1
    return str(year) + '-10-01'
        
###################### Handlers ############################

## Splits a date of the form year-month-day
def split_date(attribute, value, record, indexes) :
    dateParts = value.split('-')
    ts = time.mktime(datetime.datetime.strptime(value, "%Y-%m-%d").timetuple())
    return {attribute: value, 'timestamp' : ts, 'year' : dateParts[0], 'month': dateParts[1], 'day': dateParts[2]}

def split_coordinates(attribute, value, record, indexes) :
    return {'latitude': float(value[0]), 'longitude': float(value[1])}

def tostr(attribute, value, record, indexes) :
    return {attribute : str(value)}

def sumHistoryPrecipitation(attribute, value, record, indexes):
    if 'date_obs' in record :
        startDate = getReferenceDate(record['date_obs'])
        currentDate = record['date_obs']
    elif 'date_observation' in record :
        startDate = getReferenceDate(record['date_observation'])    
        currentDate = record['date_observation']        
    else :
        return {}
    
    startTs = time.mktime(datetime.datetime.strptime(startDate, "%Y-%m-%d").timetuple())
    endTs = time.mktime(datetime.datetime.strptime(currentDate, "%Y-%m-%d").timetuple())
    if 'nom_parcelle' in record :
        parcelle = record['nom_parcelle']
    elif 'id_parcelle' in record :
        parcelle = record['id_parcelle']
    else :
        return {}
    
    
    dryDays = 0
    rainyDays = 0
    ## To verify whether the input value has been transformed or not.
    cumulativeValue = 0.0
        
    while startTs <= endTs :
        pair = (startDate, parcelle)
        if pair in indexes['date-parcelle-merged'] :
            currentValue = float(indexes['date-parcelle-merged'][pair][attribute])
            if currentValue == 0.0 :
                dryDays += 1
            elif currentValue > 0.5 :
                rainyDays += 1
            
            cumulativeValue += currentValue
            
        startTs += 24 * 3600
        startDate = datetime.datetime.fromtimestamp(startTs).strftime('%Y-%m-%d')

    return {attribute + '_sum' : cumulativeValue, 
            attribute + '_dry_days' : dryDays, 
            attribute + '_rainy_days' : rainyDays}
    

def sumHistoryTemperature(attribute, value, record, indexes):
    if 'date_obs' in record :
        startDate = getReferenceDate(record['date_obs'])
        currentDate = record['date_obs']
    elif 'date_observation' in record :
        startDate = getReferenceDate(record['date_observation'])            
        currentDate = record['date_observation']
    else :
        return {}

    startTs = time.mktime(datetime.datetime.strptime(startDate, "%Y-%m-%d").timetuple())
    endTs = time.mktime(datetime.datetime.strptime(currentDate, "%Y-%m-%d").timetuple())
    if 'nom_parcelle' in record :
        parcelle = record['nom_parcelle']
    elif 'id_parcelle' in record :
        parcelle = record['id_parcelle']
    else :
        return {}
    
    cumulativeValue = 0.0    
    growingDayDegree = 0.0
    cumulativeAmplitude = 0.0
    tbase = 10
    while startTs <= endTs :
        pair = (startDate, parcelle)
        if pair in indexes['date-parcelle-merged'] :
            cumulativeValue += float(indexes['date-parcelle-merged'][pair][attribute])
            min = float(indexes['date-parcelle-merged'][pair]['TN'])
            max = float(indexes['date-parcelle-merged'][pair]['TX'])
            growingDayDegree += ((min + max) / 2) - tbase
            cumulativeAmplitude += (max - min)
        
        startTs += 24 * 3600
        startDate = datetime.datetime.fromtimestamp(startTs).strftime('%Y-%m-%d')

        

    return {'T_sum' : cumulativeValue, 
            'T_growing_day_degree' : growingDayDegree,
            'T_sum_amplitude' : cumulativeAmplitude}

def daysAboveMinThreshold(attribute, value, record, indexes):
    if 'date_obs' in record :
        startDate = getReferenceDate(record['date_obs'])
        currentDate = record['date_obs']
    elif 'date_observation' in record :
        startDate = getReferenceDate(record['date_observation'])            
        currentDate = record['date_observation']
    else :
        return {}
    
    startTs = time.mktime(datetime.datetime.strptime(startDate, "%Y-%m-%d").timetuple())
    endTs = time.mktime(datetime.datetime.strptime(currentDate, "%Y-%m-%d").timetuple())
    if 'nom_parcelle' in record :
        parcelle = record['nom_parcelle']
    elif 'id_parcelle' in record :
        parcelle = record['id_parcelle']
    else :
        return {}
    
    daysAboveThreshold = 0
    while startTs <= endTs :
        pair = (startDate, parcelle)
        if pair in indexes['date-parcelle-merged'] :
            temperature = float(indexes['date-parcelle-merged'][pair][attribute])
            if temperature > 11 :
                daysAboveThreshold += 1
                
        
        startTs += 24 * 3600
        startDate = datetime.datetime.fromtimestamp(startTs).strftime('%Y-%m-%d')        
        
    return {attribute + '_above_11c' : daysAboveThreshold}

def daysBelowMaxThreshold(attribute, value, record, indexes) :
    if 'date_obs' in record :
        startDate = getReferenceDate(record['date_obs'])
        currentDate = record['date_obs']
    elif 'date_observation' in record :
        startDate = getReferenceDate(record['date_observation'])            
        currentDate = record['date_observation']
    else :
        return {}    
    
    startTs = time.mktime(datetime.datetime.strptime(startDate, "%Y-%m-%d").timetuple())
    endTs = time.mktime(datetime.datetime.strptime(currentDate, "%Y-%m-%d").timetuple())
   
    if 'nom_parcelle' in record :
        parcelle = record['nom_parcelle']
    elif 'id_parcelle' in record :
        parcelle = record['id_parcelle']
    else :
        return {}
    
    daysBelowThreshold = 0
    while startTs <= endTs :
        pair = (startDate, parcelle)
        if pair in indexes['date-parcelle-merged'] :
            temperature = float(indexes['date-parcelle-merged'][pair][attribute])
            if temperature > 25 :
                daysBelowThreshold += 1
                
        
        startTs += 24 * 3600
        startDate = datetime.datetime.fromtimestamp(startTs).strftime('%Y-%m-%d')

        

    return {attribute + '_above_25c' : daysBelowThreshold}


def addHistoricalMeteo(attribute, value, record, indexes) :
    tmpDict = {}
    
    if 'nom_parcelle' in record :
        parcelle = record['nom_parcelle']
    elif 'id_parcelle' in record :
        parcelle = record['id_parcelle']
    else :
        return {}
    
    if 'date_obs' in record :
        startDate = record['date_obs']
    elif 'date_observation' in record :
        startDate = record['date_observation']         
    else :
        return {}  
    
    for i in range(4, 5) :
        tmpDict[attribute + '_sum_' + str(i) + 'w'] = calculateHistorical(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, np.sum)
        #tmpDict[attribute + '-avg-' + str(i) + 'w'] = calculateHistorical(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, np.mean)
        #tmpDict[attribute + '-min-' + str(i) + 'w'] = calculateHistorical(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, np.min)
        #tmpDict[attribute + '-max-' + str(i) + 'w'] = calculateHistorical(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, np.max)        

    result = {attribute : value}

    for key in tmpDict :
        if not math.isnan(tmpDict[key]) :
            result[key] = tmpDict[key]    
        
    return result


def addHistorical(attribute, value, record, indexes) :
    ## Add historical information about the values of the variable in the past
    tmpDict = {}
    if 'nom_parcelle' in record :
        parcelle = record['nom_parcelle']
    elif 'id_parcelle' in record :
        parcelle = record['id_parcelle']
    else :
        return {}
    
    if 'date_obs' in record :
        startDate = record['date_obs']
    elif 'date_observation' in record :
        startDate = record['date_observation']         
    else :
        return {}
    
    for i in range(4, 5) :
        #print('Calculating historical data ' + attribute + '-avg-' + str(i) + 'w')
        tmpDict[attribute + '_avg_' + str(i) + 'w'] = calculateHistorical(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, np.mean)
        #print('Calculating historical data ' + attribute + '-avg-pente-' + str(i) + 'w')
        #tmpDict[attribute + '-avg-pente-' + str(i) + 'w'] = calculateHistoricalSlope(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, meanSlope)
        #print('Calculating historical data ' + attribute + '-max-pente-' + str(i) + 'w')        
        #tmpDict[attribute + '-max-pente-' + str(i) + 'w'] = calculateHistoricalSlope(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, maxSlope)        
        #tmpDict[attribute + '-max-' + str(i) + 'w'] = calculateHistorical(attribute, value, indexes['date-parcelle'], startDate, parcelle, 7 * i, np.max)        

    result = {attribute : value}
    
    for key in tmpDict :
        if not math.isnan(tmpDict[key]) :
            result[key] = tmpDict[key]

        
    return result
  
##########################################################    

## Reads the metadata about the attributes from a TSV file.
def readSchemaFile(fileName) :
    schema = {}
    cols = []
    with open(fileName, 'r') as schemaF :
        lineN = 1
        for line in schemaF.readlines() :
            cleanLine = line.rstrip('\n')
            if lineN == 1 :
                cols = cleanLine.split('\t')
            else :
                values = cleanLine.split('\t')
                if len(values) < len(cols) :
                    print('Line ' + str(lineN) 
                           + ' does not have enough values (' + str(len(values)) 
                           + ' found, ' + str(len(cols)) + ' expected)', file=sys.stderr)
                    continue
                
                attribute = values[0]
                schema[attribute] = {}
                for i in range(1, len(cols)) :
                    schema[attribute][cols[i]] = values[i]
                
            lineN = lineN + 1        
        print(str(lineN) + ' lines parsed from ' + fileName)
    return schema


def nestHandlers(handlers, value, attribute, attributeInfo) :
    if type(value) == str :
        evalStr = "'" + value + "'"
    else :
        evalStr = str(value)
    
    for handler in handlers :
        evalStr = handler + "(" + "'" + attribute + "', "  +  evalStr + ", record, index)"

    return evalStr

def applyHandlers(record, attribute, schema, index):
    handlersText = schema[attribute]['handlers']
    attributeValue = record[attribute]
    
    if handlersText != '-' :
        handlersList = handlersText.split(',')
        expression = nestHandlers(handlersList, attributeValue, attribute, schema[attribute])                        
        evaluatedExpression = eval(expression)
    else :
        evaluatedExpression = {attribute : attributeValue}
        
    return evaluatedExpression

def extendRecordAndSchema(attribute, newFields, originalSchema, record, extendedSchema):
    for newAttribute in newFields :
        record[newAttribute] = newFields[newAttribute]
        if newAttribute not in extendedSchema :
            extendedSchema[newAttribute] = {'newtype' : type(record[newAttribute]), 
                                            'schema' : originalSchema[attribute]}

def parseJSONFile(fileName, schema) :
    data = []
    extendedSchema = {}
    indexDateParcelle = {}
    with codecs.open(fileName, 'r', encoding='ascii', errors='ignore') as ifile :
        rawData = json.load(ifile, encoding='ascii')
        
        ## Index the data
        indexDateParcelle['date-parcelle'] = {}
        for record in rawData :
            newRecord = {}
            for attribute in record['fields'] :
                newRecord[attribute] = record['fields'][attribute]
                
            key1 = record['fields']['date_obs']
            key2 = record['fields']['nom_parcelle']
            
            if key1 not in indexDateParcelle['date-parcelle'] :
                indexDateParcelle['date-parcelle'][key1] = {}
                            
            if key2 not in indexDateParcelle['date-parcelle'][key1] :
                indexDateParcelle['date-parcelle'][key1][key2] = {}
            
            indexDateParcelle['date-parcelle'][key1][key2] = newRecord

        print(len(rawData), " records read")   
        ## Final pass on the data        
        for record in rawData :
            newRecord = {}
            for attribute in record['fields'] :
                if attribute in schema and schema[attribute]['store'] == 'yes' :
                    additionalFields = applyHandlers(record['fields'], attribute, schema, indexDateParcelle)
                    extendRecordAndSchema(attribute, additionalFields, schema, newRecord, extendedSchema)
        
            data.append(newRecord)            
        
    return data, extendedSchema
    
def arffOutput(data, extendedSchema, directoryPrefix) :
    for dataset in data :
        fileNameFull = directoryPrefix + dataset + '.arff'
        #print(fileNameFull)
        with open(fileNameFull, 'w') as fout :    
            fileNameOnlyStr = directoryPrefix + dataset + '_only_symbolic.arff'
            #print(fileNameOnlyStr)
            with open(fileNameOnlyStr, 'w') as foutOnlyStr :       
                fout.write('@relation ' + dataset + '\n\n')
                foutOnlyStr.write('@relation ' + dataset + '\n\n')
                
                sortedAttributes = sorted(extendedSchema[dataset])
                for attribute in sortedAttributes :
                    if extendedSchema[dataset][attribute]['schema']['category'] == 'symbolic' :
                        foutOnlyStr.write('@attribute ' + attribute + ' ')
                        foutOnlyStr.write('string\n')
                        fout.write('@attribute ' + attribute + ' ')
                        fout.write('string\n')
                    else:                
                        fout.write('@attribute ' + attribute + ' ')
                        fout.write('numeric\n')

                 
                fout.write('\n')
                fout.write('@data\n')
                foutOnlyStr.write('\n')
                foutOnlyStr.write('@data\n')
                
                for record in data[dataset] :
                    line = []
                    lineOnlyStr = []
                    for attribute in sortedAttributes :
                        if extendedSchema[dataset][attribute]['schema']['category'] == 'symbolic' :
                            if attribute not in record :
                                lineOnlyStr.append('?')
                            else :
                                lineOnlyStr.append(str(record[attribute]))
                        
                        if attribute not in record :
                            line.append('?')
                        else :
                            line.append(str(record[attribute]))
                    
                    fout.write(",".join(line) + '\n')
                    foutOnlyStr.write(",".join(lineOnlyStr) + '\n')

def csvOutput(data, extendedSchema, directoryPrefix) :
    for dataset in data :
        fileNameFull = directoryPrefix + dataset + '.csv'
        #print(fileNameFull)
        with open(fileNameFull, 'w') as fout :    
            fileNameOnlyStr = directoryPrefix + dataset + '_only_symbolic.csv'
            #print(fileNameOnlyStr)
            with open(fileNameOnlyStr, 'w') as foutOnlyStr :       
                sortedAttributes = sorted(extendedSchema[dataset])                
                qualifiedSortedAttributesOnlyStr = []
                qualifiedSortedAttributes = []
                for attribute in sortedAttributes :
                    if extendedSchema[dataset][attribute]['schema']['category'] == 'symbolic' :
                        qualifiedSortedAttributes.append(attribute + '@string')
                        qualifiedSortedAttributesOnlyStr.append(attribute + '@string')
                    else:                
                        qualifiedSortedAttributes.append(attribute + '@numeric')

                fout.write(','.join(qualifiedSortedAttributes))
                fout.write('\n')
                foutOnlyStr.write(','.join(qualifiedSortedAttributesOnlyStr))
                foutOnlyStr.write('\n')
                                 
                for record in data[dataset] :
                    line = []
                    lineOnlyStr = []
                    for attribute in sortedAttributes :
                        if extendedSchema[dataset][attribute]['schema']['category'] == 'symbolic' :
                            if attribute not in record :
                                lineOnlyStr.append('?')
                            else :
                                lineOnlyStr.append(str(record[attribute]))
                        
                        if attribute not in record :
                            if extendedSchema[dataset][attribute]['schema']['category'] == 'numerical' :
                                line.append('?!')
                            else :
                                line.append('?')                                
                        else :
                            line.append(str(record[attribute]))

                    # Remove the lines that lack at least one numerical value
                    if not '?!' in line :                   
                        fout.write(",".join(line) + '\n')
                    
                    foutOnlyStr.write(",".join(lineOnlyStr) + '\n')


def indexMeteo(meteoFilePath, key1, key2):
    index = {'date-parcelle-merged' : {}, 'date-parcelle': {}}
    with codecs.open(meteoFilePath, 'r', encoding='ascii', errors='ignore') as fmeteo :
        csvReader = csv.DictReader(fmeteo, delimiter=';')
        for row in csvReader :
            # Index on pairs date, nom_parcelle
            date = row[key1]
            parcelle = row[key2]            
            index['date-parcelle-merged'][(date, parcelle)] = row
            
            if date not in index['date-parcelle'] :
                index['date-parcelle'][date] = {}
                
            if parcelle not in index['date-parcelle'][date] :
                index['date-parcelle'][date][parcelle] = row
    
    #print(index)
    return index

def joinRecord(dateKey, parcelleKey, meteoIndex, schema, extendedSchema, record) :
    for attribute in meteoIndex['date-parcelle'][dateKey][parcelleKey] :
        if attribute not in record :
            ## Extend the record with new attributes
            record[attribute] = meteoIndex['date-parcelle'][dateKey][parcelleKey][attribute]
            if attribute in schema and schema[attribute]['store'] == 'yes' :
                newFields = applyHandlers(record, attribute, schema, meteoIndex)
                extendRecordAndSchema(attribute, newFields, schema, record, extendedSchema)
    

def joinIFVWithMeteoPieges(data, extendedSchema, meteoIndex, schema) :
    for record in data :
        dateKey = str(record['year']) + '-' + str(record['month']) + '-' + str(record['day'])
        if 'nom_parcelle' in record :
            trials = 0
            nomParcelle = record['nom_parcelle']
            while trials < 6 :
                #print(dateKey, nomParcelle)
                if dateKey in meteoIndex['date-parcelle'] and nomParcelle in meteoIndex['date-parcelle'][dateKey]  : 
                    #print("Eureka")           
                    joinRecord(dateKey, nomParcelle, meteoIndex, schema, extendedSchema, record)
                    break                                        
                else :    
                    timeStamp = time.mktime(datetime.datetime.strptime(dateKey, "%Y-%m-%d").timetuple())
                    timeStamp = timeStamp - 3600 * 24
                    dateKey = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d')
                trials = trials + 1


def joinIFVWithMeteo(data, extendedSchema, meteoIndex, schema):    
    for record in data :
        dateKey = str(record['year']) + '-' + str(record['month']) + '-' + str(record['day'])
        # First join with the meteo
        if 'nom_parcelle' in record :
            if dateKey in meteoIndex['date-parcelle'] and record['nom_parcelle'] in meteoIndex['date-parcelle'][dateKey]   :
                # Add the additional information
                joinRecord(dateKey, record['nom_parcelle'], meteoIndex, schema, extendedSchema, record)
        else :
            print(record, 'has incomplete information')

def loadWineData() :
    # Data type declarations
    dataFolder = '../../data/ifv/original-json'
#    dataFolder = '../data/ifv/test'
    # Meteorological data
    meteoFile = '../../data/meteo/weather_EPICURE_MALADIE.csv'
    
    ## First read the schema.
    ## This is a given as a dict with the key is the name of the attribute
    ## and the value is a dict with information such its data type, its parsing handler
    ## (python function that should parse the data) and whether this attribute will be
    ## predicted by our methods.
    startTime = time.process_time()
    print('Reading the schema under schema-ifv.tsv')
    schema = readSchemaFile('schema-ifv.tsv')
    print('Reading the schema took:', time.process_time() -  startTime, 'seconds')
    print('Building index for the meteorological data')
    meteoIdx = indexMeteo(meteoFile, 'date', 'NOM_PARCELLE')
        
    data = {}    
    extendedSchema = {}
    for file in os.listdir(dataFolder) :
        if file.endswith('.json') :
            fullFile = dataFolder + '/' + file
            
            print('Parsing ' + fullFile)
            datasetName = file.rstrip('.json')
            startTime= time.process_time()
            data[datasetName], extendedSchema[datasetName] = parseJSONFile(fullFile, schema)
            print('Parsing of', fullFile, 'took', time.process_time() - startTime, 'seconds')
            
            
            print("Adding meteorological information to ", fullFile)
            startTime = time.process_time()
            if 'pieges' in file :
                joinIFVWithMeteoPieges(data[datasetName], extendedSchema[datasetName], meteoIdx, schema)
            else :
                joinIFVWithMeteo(data[datasetName], extendedSchema[datasetName], meteoIdx, schema)
            
            print('Joining with meteo information took',  time.process_time() - startTime, 'seconds')

    return data, extendedSchema

def parseCSVFile(fullFile, schema):
    data = []
    extendedSchema = {}
    indexDateParcelle = {}
    
    with codecs.open(fullFile, 'r', encoding='ascii', errors='ignore') as fdata :
        rawData = csv.DictReader(fdata, delimiter=',')
        indexDateParcelle['date-parcelle'] = {}
        
        nRecords = 0
        tmpRawData = []
        for record in rawData :
            key1 = record['date_observation']
            key2 = record['id_parcelle']
            if key1 not in indexDateParcelle['date-parcelle'] :
                indexDateParcelle['date-parcelle'][key1] = {}
                             
            if key2 not in indexDateParcelle['date-parcelle'][key1] :
                indexDateParcelle['date-parcelle'][key1][key2] = {}
             
            indexDateParcelle['date-parcelle'][key1][key2] = record
            nRecords += 1
            tmpRawData.append(record)
        
        for record in tmpRawData :
            newRecord = {}             
            for attribute in record :
                if attribute in schema and schema[attribute]['store'] == 'yes' :
                    additionalFields = applyHandlers(record, attribute, schema, indexDateParcelle)
                    extendRecordAndSchema(attribute, additionalFields, schema, newRecord, extendedSchema)
            
            data.append(newRecord)    
                        
        print(len(data), "records read")
        
    return data, extendedSchema    

def joinVigiculturesWithMeteo(data, extendedSchema, meteoIndex, schema):
    for record in data :
        dateKey = str(record['year']) + '-' + str(record['month']) + '-' + str(record['day'])
        # First join with the meteo
        if 'id_parcelle' in record :
            if dateKey in meteoIndex['date-parcelle'] and record['id_parcelle'] in meteoIndex['date-parcelle'][dateKey] :
                # Add the additional information
                joinRecord(dateKey, record['id_parcelle'], meteoIndex, schema, extendedSchema, record)
        else :
            print(record, 'has incomplete information')


def joinVigiculturesWithResistance(data, extendedSchema, resistanceIdx, schema):
    extendedSchema['resistance'] = {'newtype' : str, 
                                    'schema' : schema['resistance']}
    for record in data:
        yr = int(record['year'])
        vrt = record['variete']
        recordKeys = [(vrt, yr), (vrt, yr + 1), (vrt, yr + 2)]
        for recordKey in recordKeys :            
            if recordKey in resistanceIdx :
                record['resistance'] = resistanceIdx[recordKey]['txt_valeur']
                break    

def indexResistance(resistanceFileName):
    index = {}
    key1 = 'nom_variete'
    key2 = 'serie_annee'
    with codecs.open(resistanceFileName, 'r', encoding='ascii', errors='ignore') as fresistance :
        csvReader = csv.DictReader(fresistance, delimiter=';')
        for row in csvReader :
            # Index on variety and year
            variety = row[key1]
            year = int(row[key2])            
            index[(variety, year)] = row
    
    return index
    

def loadWheatData() :
    dataFolder = '../../data/vigicultures/csv'
    meteoFile = '../../data/meteo/weather_Vigiculture.csv'    
    resistanceFile = '../../data/vigicultures/variete-resistance-VARCOM/20180404_varcomRJ_BT_hist.csv'
    
    startTime = time.process_time()
    print('Reading the schema under schema.tsv')
    schema = readSchemaFile('schema-vigicultures.tsv')
    print('Reading the schema took:', time.process_time() -  startTime, 'seconds')
    print('Building index for the meteorological data')
    meteoIdx = indexMeteo(meteoFile, 'date', 'id_parcelle')
    resistanceIdx = indexResistance(resistanceFile)
        
    data = {}    
    extendedSchema = {}
    for file in os.listdir(dataFolder) :
        if file.endswith('.csv') :
            fullFile = dataFolder + '/' + file
            print('Parsing ' + fullFile)
            datasetName = file.rstrip('.json')
            startTime= time.process_time()
            data[datasetName], extendedSchema[datasetName] = parseCSVFile(fullFile, schema)            
            print('Parsing of', fullFile, 'took', time.process_time() - startTime, 'seconds')
            
            print("Adding meteorological information to ", fullFile)
            startTime = time.process_time()
            joinVigiculturesWithMeteo(data[datasetName], extendedSchema[datasetName], meteoIdx, schema)            
            print('Joining with meteo information took',  time.process_time() - startTime, 'seconds')
            
            print("Adding information about wheat variety resistance")
            startTime = time.process_time()
            joinVigiculturesWithResistance(data[datasetName], extendedSchema[datasetName], resistanceIdx, schema)
            print('Joining with resistance information took',  time.process_time() - startTime, 'seconds')
            
            
    return data, extendedSchema


if __name__ == "__main__" : 
    if 'wine' in sys.argv :
        data, extendedSchema = loadWineData()
        print('Outputting the wine data')
        startTime = time.process_time()
        arffOutput(data, extendedSchema, '../../data/ifv/processed/')
        csvOutput(data, extendedSchema, '../../data/ifv/processed/')
        print('Outputting took', time.process_time() - startTime, 'seconds')
    
    if 'wheat' in sys.argv :
        data, extendedSchema = loadWheatData()
        print('Outputting the wheat data')
        startTime = time.process_time()
        arffOutput(data, extendedSchema, '../../data/vigicultures/processed/')
        csvOutput(data, extendedSchema, '../../data/vigicultures/processed/')        
        print('Outputting took', time.process_time() - startTime, 'seconds')