package org.fr.inria.lacodam.patternMining.tests;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

import ca.pfv.spmf.algorithms.frequentpatterns.lcm.AlgoLCM;
import ca.pfv.spmf.algorithms.frequentpatterns.lcm.Dataset;
import ca.pfv.spmf.tools.dataset_converter.TransactionDatabaseConverter;
import ca.pfv.spmf.tools.resultConverter.ResultConverter;

public class LCMFrequentPatternsTest {
	
	public static void main(String [] arg) throws IOException{
		URL url = LCMFrequentPatternsTest.class.getResource("bsv_vigne_maladies_epicure_only_symbolic.arff");		
		TransactionDatabaseConverter converter = new TransactionDatabaseConverter();
		Map<Integer, String> mapping = converter.convertARFFandReturnMap(url.getPath(), url.getPath() + ".converted", Integer.MAX_VALUE);
		String input = java.net.URLDecoder.decode(url.getPath() + ".converted", "UTF-8");

		double minsup = 0.005; 
		Dataset dataset = new Dataset(input);
		
		// Applying the algorithm
		AlgoLCM algo = new AlgoLCM();
		// if true in next line it will find only closed itemsets, otherwise, all frequent itemsets
		algo.runAlgorithm(minsup, dataset, ".//output");
		algo.printStats();

		ResultConverter rConverter = new ResultConverter();
		rConverter.convert(mapping, ".//output", "final_output", Charset.defaultCharset());
	}
}
