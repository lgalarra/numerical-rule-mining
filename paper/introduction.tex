In the golden age of data, accurate numerical prediction models are of great utility in absolutely all disciplines.
The task of predicting a numerical variable of interest from the values of other variables---called features---is known as regression analysis and the literature
is rich in this respect~\cite{piecewise-regression, cpxr, model-trees-with-splitting-nodes, regression-trees, boosting-trees, model-trees}. 
As data steadily complexifies, and the need 
for interpretable methods becomes compelling~\cite{survey-interpretability}, a line of research in regression analysis focuses 
on inducing interpretable prediction models
on heterogenous data. By \emph{interpretable}, we mean models that provide a compact and comprehensible
explanation of the interaction between 
the features and the target variable, e.g., a linear regression. 
By \emph{heterogeneous} data, we mean
data that can be hardly modeled by a single global regression function, but instead by a set 
of local models applicable to subsets of the data. The most prominent methods in this line are piecewise regression (PR, also called segmented regression)~\cite{piecewise-regression}, regression trees (RT)~\cite{regression-trees}, model trees (MT)~\cite{model-trees, model-trees-with-splitting-nodes} and contrast pattern-aided regression
(CPXR)~\cite{cpxr}. All these approaches mine \emph{hybrid rules} on tabular data such as the example in Table~\ref{table:example}. 
A hybrid rule is a statement of the form $p \Rightarrow y = f(X)$ where $p$ 
is a logical 
expression on categorical features such as $p : \mathit{property\text{-}type}=``\mathit{cottage}"$,
and $y = f(X)$ 
is a regression model for a numerical variable of interest, e.g., 
$\mathit{price} = \alpha + \beta \times \mathit{rooms} + \gamma \times \mathit{surface}$
that applies only to the region characterized by $p$, for instance, $\{x^1, x^2, x^3\}$ in Table~\ref{table:example}.
% Moreover, $y = f(X)$ 
% is a regression model for a numerical variable of interest, e.g., 
% $\mathit{price} = \alpha + \beta \times \mathit{rooms} + \gamma \times \mathit{surface}$
% that applies only to the region defined by $p$.
%Even regression and model trees, which do not technically mine hybrid rules, can be converted to rules if we enumerate every path
%from the root to a leaf as in Figure~\ref{fig:regression-tree}. 
% Even regression and model trees, which strictly speaking do not mine hybrid rules, can be converted to rules if we enumerate every path
% from the root to a leaf. 

The advantage of methods based on hybrid rules is that they 
deliver statements that can be easily interpreted by humans. In contrast, they 
usually lag behind black-box methods such as gradient boosting trees~\cite{boosting-trees} or 
random forests~\cite{random-forests} in terms of prediction power. While this  
interpretability-accuracy trade-off seems ineluctable, in this work we show that it can be tilted towards 
the side of interpretability without compromising performance. This can be achieved by exploring the space of data regions
in a hierarchical fashion.
Indeed, existing % \commenta{Added ``existing'' to avoid having our method in the same category} 
pattern-based regression methods carry out 
a fully \emph{greedy} exploration  
of the space of data regions, which leads to more and longer rules.
%which deepens the interpretability-accuracy trade-off. 
%\commenta{Hum, this is not really a claim: either they do it or not, this can be easily verified in their code. The claim could be, for example, that this greedy exploration is detrimental for their performance, and that it can be improved without sacrificing too much computation time.}
Consider for example Figure~\ref{fig:regression-tree-exploration} depicting two steps in 
the search exploration
of a regression tree learner. At each step, the learning algorithm splits the regions of interest 
into two parts based on the 
condition that delivers the most intra-homogeneous partitioning of the data points w.r.t. the target variable. 
This locally optimal decision may, however, make the learner miss other highly homogeneous subsets of the data. As a matter of fact, if such regions 
span across the boundaries of a split, 
they will never be tested for potentially accurate rules. That is the case of the  
dashed rectangle in the left diagram of Figure~\ref{fig:regression-tree-exploration} that can be characterized by the single condition 
$\textit{state}=``\textit{excellent}"$ (Figure~\ref{fig:hipar-exploration}), but is 
split by a tree learner into two more complex patterns, i.e.,
$ptype=``\mathit{cottage}" \land \mathit{stage}\neq``\textit{v. good}"$ and $ptype\neq``\mathit{cottage}" \land \textit{state}\neq``\mathit{good}"$.
%by a tree learner.
%(right-hand side diagram in Figure~\ref{fig:regression-tree-exploration}).  
%XXXAlex: in figure dotted line looks like normal line (dots are too close). Prefer a dashed lone?
This happens because regression trees are greedy: splitting steps are based on locally optimal criteria that are never reexamined.

The hypothesis of this work is that a hierarchical exploration of the 
space of regions, as depicted in Figure~\ref{fig:hipar-exploration}, can find simpler and smaller sets of rules that are still accurate. Each square in Figure~\ref{fig:hipar-exploration} represents a data region characterized
by a condition on a categorical feature. Regions can overlap and we can recursively explore their sub-regions. 
% This condition can use either a categorical variable (e.g., the variety in the example) 
% or a numerical feature (e.g., the precipitation) that has been discretized. 
Once we have identified ``interesting'' zones in the data, 
we can learn hybrid rules on those regions and select the best ones
in terms of coverage and joint prediction accuracy for a target variable.
This principle summarizes the HIPAR approach proposed in this paper. 
% Given a dataset with categorical and 
% numerical attributes, and a target numerical variable, 
% HIPAR returns a set of accurate hybrid rules for the target variable such that each rule applies to a data region and,
% the ensemble covers the entire dataset.  
But before elaborating on our method, we introduce some relevant concepts in the next section.

% \begin{table*}[t]
% 	\centering
% 	\footnotesize
% 	\caption{Toy example for the prediction of real estate prices based on the attributes of the property. The colors
% 	red, orange, and green denote high, medium, and low prices respectively.}
% 	%\vspace{-0.3cm}
% 	\begin{tabular}{>{\centering\arraybackslash}m{0.3cm}>{\centering\arraybackslash}m{2.2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{1cm}>{\centering\arraybackslash}m{1.5cm}>{\centering\arraybackslash}m{1cm}}
% 		\emph{id} 		   & \emph{property-type} 	    & \emph{state}  	         & \emph{rooms} 	    & \emph{surface} 	               & \emph{price}	\\ 
% 		\toprule		
% 		{\color{red}$x^1$} 	  & {\color{red}cottage}      & {\color{red}very good}	     &\color{red}5          & \color{red}120        		   & \color{red}510k               \\ 
% 		{\color{red}$x^2$}	  & \color{red}cottage        & \color{red}excellent         &\color{red}3          & \color{red}55        		       & \color{red}410k               \\
% 		{\color{orange}$x^3$}	  & \color{orange}cottage     & \color{orange}excellent 	 &\color{orange}3       & \color{orange}50          	   & \color{orange}350k              \\ 
% 		{\color{orange}$x^4$} 	  & \color{orange}apartment   & \color{orange}excellent	     &\color{orange}5       & \color{orange}85          	   & \color{orange}320k              \\ 
% 		{\color{OliveGreen}$x^5$} & \color{OliveGreen}apartment    & \color{OliveGreen}good		     &\color{OliveGreen}4        & \color{OliveGreen}52         		   & \color{OliveGreen}140k              \\
% 		{\color{OliveGreen}$x^6$} & \color{OliveGreen}apartment    & \color{OliveGreen}good		     &\color{OliveGreen}3        & \color{OliveGreen}45        	       & \color{OliveGreen}125k               \\ \bottomrule
% 	\end{tabular} 	
% 	%\vspace{-0.5cm}
% 	\label{table:example}
% \end{table*}

\begin{table}[t]
	\centering
%	\footnotesize
	\caption{Toy example for the prediction of real estate prices based on the attributes of the property. The symbols
	*, +, - denote high, medium, and low prices respectively.}
	%\vspace{-0.3cm}
	\begin{tabular}{>{\centering\arraybackslash}m{0.3cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{1.5cm}>{\centering\arraybackslash}m{0.8cm}>{\centering\arraybackslash}m{0.8cm}>{\centering\arraybackslash}m{1.2cm}}
	      \emph{id} 	& \emph{property-type} 	    	& \emph{state}   & \emph{rooms} & \emph{surface}	& \emph{price}	\\ 
		\toprule		
		{$x^1$} 	& {cottage}      		& very good	 &5         	& 120        		& 510k (*)              \\ 
		{$x^2$}	  	& cottage        		& very good    	 &3         	& 55        		& 410k (*)              \\
		{$x^3$}	  	& cottage     			& excellent 	 &3       	& 50          	   	& 350k (+)             \\ 
		{$x^4$} 	& apartment   			& excellent	 &5       	& 85          	   	& 320k (+)             \\ 
		{$x^5$} 	& apartment    			& good		 &4       	& 52         		& 140k (-)             \\
		{$x^6$} 	& apartment    			& good		 &3        	& 45        	       	& 125k (-)              \\ \bottomrule
	\end{tabular} 	
	%\vspace{-0.5cm}
	\label{table:example}
\end{table}



\begin{figure*}
\begin{minipage}[t]{0.48\linewidth}
  \centering
    \includegraphics[width=\linewidth]{figures/decision-tree-splitting.pdf}
    \captionof{figure}{Two steps of the exploration of regions by a regression tree learner induced on Table \ref{table:example}.}
    \label{fig:regression-tree-exploration}
\end{minipage}
\hspace{0.03\linewidth}
\begin{minipage}[t]{0.48\linewidth}
%\begin{figure}
  %\caption{A picture of a gull.}
  \centering
    \includegraphics[width=\linewidth]{figures/hipar-splitting.pdf}
%    \captionof{figure}{Two steps of a hierarchical exploration: first-level zones can overlap. In the next step, the exploration extends to sub-zones.}
    \captionof{figure}{Two steps of a hierarchical exploration.}
    
    \label{fig:hipar-exploration}
%\end{figure}
\end{minipage}
\end{figure*}

% \begin{figure*}
%   \centering
%     \includegraphics[width=\textwidth]{figures/decision-tree-splitting.pdf}
%     \caption{Two steps of the exploration of regions by a regression tree learner induced on Table \ref{table:example}.}
%     \label{fig:regression-tree-exploration}
% \end{figure*}
% 
% \begin{figure*}
%   \centering
%     \includegraphics[width=\textwidth]{figures/hipar-splitting.pdf}
%     \caption{Two steps of a hierarchical exploration: first-level zones can overlap. In the next step, the exploration extends to sub-zones.}
%     \label{fig:hipar-exploration}
% \end{figure*}
