Having introduced a common notation, we now revisit the state-of-the-art in pattern-aided regression.
%We first elaborate on the existing solutions for this task. 
Furthermore, we discuss about two related paradigms for data analysis, namely subgroup discovery (SD) and exceptional model mining (EMM). 

%\subsection{Pattern-aided Regression} \label{subsec:par} \\
% %\\
% Pattern-aided regresion methods can learn a set of regression models that are valid in regions of the data. We present three categories
% of methods in this line.

\subsection{Piecewise Regression (PR).}
\cite{piecewise-regression} is among the first approaches for pattern-aided regression. PR splits the domain 
of one of the numerical variables, called the \emph{splitting variable},  
into segments such that the dataset 
regions defined by those segments exhibit a good linear fit for a \emph{target variable}. 
The splitting variable must be either ordinal\footnote{A special type of categorical attribute on which a total order on the values of its domain 
can be defined.} or numerical.
The regions are constructed via bottom-up 
hierarchical agglomerative clustering:   
Starting with clusters of size $\theta$, this bottom-up approach greedily picks the segment with the smallest 
residual average of squares, and fix it for the next iteration while \emph{declustering} the remaining points. 
Fixed clusters can be extended by incorporating adjacent points and other adjacent fixed clusters. The process stops when the number of isolated points drops below a threshold
or the goodness of fit does not improve with subsequent merging steps. 

Other variants of PR, such as~\cite{flirti}, focus on detecting regions of the space where the target variable correlates with polynomials of degree $n\neq1$ on the input features. That includes regions where we can predict a constant value for the target ($n=0$ as the target does not correlate with the input features), or regions where polynomial regression is required ($n>1$).

%In~\cite{piecewise-regression} the authors propose to stop the iterative process when the number of isolated points drops below a threshold
%or the goodness of fit does not improve with subsequent merging steps.
PR usually outperforms single linear models on data with a multimodal distribution, 
however its limitations are manyfold. Firstly, it can only split the dataset based on one attribute at a time. 
Secondly, it cannot characterize data regions in terms of arbitrary categorical attributes. Thirdly, its greedy strategy does not guarantee
to find the best possible segmentation of the data~\cite{piecewise-regression}. 
PR models can be seen as sets of hybrid rules $z \in [v_1, v_2] \Rightarrow y = f(X)$, 
where the antecedent is an interval constraint on the splitting variable $z$.

\subsection{Tree-based Methods}
A regression tree~\cite{regression-trees} (RT) is a decision tree such that its leaves predict a numerical variable. 
Like decision trees,
RTs are constructed in a top-down fashion. At each step, the data is 
partitioned into two regions according to the condition that maximizes the intra-homogeneity 
of the resulting subsets w.r.t. the target variable (e.g., Figure~\ref{fig:regression-tree-exploration}). 
The conditions are defined on categorical and discretized numerical attributes.
This process is repeated while the subsets are large enough and its goodness of fit still improvable, otherwise
the learner creates a leaf that predicts the average of 
the target variable in the associated data region.
% If a subset is large enough and its goodness of fit can still be improved, the splitting process is repeated on the subset,
% otherwise a leaf with a regression model for the target variable is produced.
%This process can result in trees like the one depicted in Figure~\ref{fig:regression-tree}.
%Predictions on new observations are obtained by following a path from the root to a leaf depending on the met conditions.
% For RT~\cite{regression-trees}, the model in the leaf of an RT is a constant function, e.g., the average of the values of 
% the target variable in the data region associated to the leaf. 
Model trees (MT)~\cite{model-trees, model-trees-with-splitting-nodes} associate linear functions to the leaves of the tree. 

We can mine hybrid rules from RT and MT 
if we enumerate every path from the root to a leaf (or regression node in~\cite{model-trees}) as depicted in Figure~\ref{fig:regression-tree}.
Unlike piecewise regression, RT and MT do exploit categorical features. Yet, their construction obeys 
a greedy principle: Data is split according to the criterion that maximizes the goodness of fit at a particular stage, 
and steps cannot be undone. This makes RT and MT prone to overfitting when not properly parameterized.  
More accurate methods such as random forests (RF) reduce this risk by learning tree ensembles that model the \emph{whole picture}. Alas, RF are not interpretable. While some approaches~\cite{interpretable-rf, extracting-rules-from-rf} can extract representative rules from RF, they sacrifice accuracy. 
% If a
% region with a better goodness of fit spans across the subsets of a split, RTs and MTs 
% will not be able to find it as they do not undo splits. 

Our experiments in Section~\ref{sec:evaluation} confirm that 
RT and MT can make too many splitting steps yielding large and complex sets of rules,
even though we can attain a performance comparable to RF with fewer rules. 
% We show 
% Compact and legible trees, on the contrary, may exhibit significantly lower accuracy. 
% Besides, experience shows that this family of models tend to overfit the data~\cite{reference-about-overfitting}
% \commentl{Alexandre, Olivier: do you know a reference that I can add to support this statement}.

\begin{figure}
	\centering
    \includegraphics[width=\linewidth]{figures/regression-tree.pdf}
    \caption{Regression tree learned to predict the price in Table~\ref{table:example}. Paths from the root to the leaves are hybrid rules.}
   \label{fig:regression-tree}
\end{figure}

\subsection{Contrast Pattern-Aided Regression.}
\cite{cpxr} proposes CPXR, a method that mines hybrid rules on the regions 
of the input dataset where a global linear regression model performs poorly. 
% CPXR resorts to contrast pattern mining for this purpose. 
% The approach works by first inducing a global regression model on the whole dataset. 
% The dataset
First, CPXR splits the dataset
into two classes consisting of the data points where the global model yielded a 
large (LE) and a small error (SE) respectively. Based on this partitioning,
CPXR discretizes the numerical variables and mines contrast patterns~\cite{contrast-patterns} that characterize the difficult class LE. 
The algorithm then induces hybrid rules on the regions defined by those patterns.
After an iterative selection process, 
the approach reports a small set of hybrid rules with 
low overlap and good error reduction w.r.t. the global regression model. 
This set includes also a \emph{default model} 
induced on those points not covered by any rule.
Prediction for new cases in CPXR is performed by weighting the answers of all the rules that apply.  
The weights depend on the error reduction of the rules w.r.t. the global regression model. 
% Constrast pattern-aided regression usually boosts the performance of the reference global regression model thanks 
% to its search for strongly correlated subregions with high error drop. 

Despite its error-reduction-driven selection for rules, CPXR iterative search strategy is still greedy in nature.
%Once the data has been split in the initial step, regions spanning across the classes LE and SE are disregarded.
Moreover, regions spanning across the classes LE and SE are disregarded, discretization is done once, and 
the search is restricted to the class of contrast patterns of the LE class 
(ignoring any error reduction in SE). While the latter decision keeps the 
search space under control, our experiments show that exploring the (larger) class of closed patterns allows 
for a significant gain in prediction accuracy with a reasonable penalty in runtime. 

\subsection{Related Paradigms.} \label{subsec:sd-emm}
The problem of finding data regions with a high goodness of fit for a target variable is similar to the problem of subgroup discovery~\cite{sd} (SD). In its general formulation, 
SD reports subgroups---data regions in our jargon---where the behavior of the target variable deviates notably 
from the norm. There exist plenty of SD approaches~\cite{sd-survey} tailored for different subgroup description languages, 
different types of variables and different notions of ``exceptionality''. For example,~\cite{sd} studies discretization techniques 
to deal with numerical attributes in the subgroup descriptions, and shows the application of SD in diabetes diagnosis and fraud detection, i.e., to find the characterizations of subgroups with high incidence of diabetes and high fraud rate in mail order data. 

A more general framework, called Exceptional Model Mining (EMM)~\cite{emm,emm-cook-distance}, 
extends the notion of exceptionality to arbitrary sets of target variables. In this spirit EMM can find exceptional groups where the joint distribution of the target variables 
in a subgroup differs greatly from the global joint distribution. %, by for instance, comparing the topology of the local bayesian network with the network learned on the whole dataset.
Finding exceptionally well-correlated subgroups in data can be framed as a SD or EMM task, nonetheless, these paradigms are concerned 
with reporting subgroups that are \emph{individually exceptional}. For this reason, EMM and SD methods are usually 
greedy and resort to strategies such as beam search to find a small set of exceptional subgroups. 
Conversely, we search for hybrid rules that are \emph{jointly exceptional}, that is, 
they (i) explain the whole dataset, and (ii) they jointly achieve good performance. 
While methods such as~\cite{sd} propose an average exceptionality score for sets of subgroups, 
such a simple score does not capture the requested synergy between sets of hybrid rules. 
Indeed, our experimental section shows that a SD-like selection strategy for hybrid rules yields 
lower performance gains than the strategy proposed in this paper.     

